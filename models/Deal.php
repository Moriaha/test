<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use app\models\Lead;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{	

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }
		
	public function getLeadItem()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
	
		public function getDealName()
    {
        return $this->name;
    }
	

	
	
	public static function getLeads()
	{
		$allleads = self::find()->all();
		$allLeadsArray = ArrayHelper::
					map($allleads, 'leadId', 'leadId');
//		$all=Lead::Leads();
		
		
//		$lead = []; 
//		foreach($allLeadsArray as $leadId => $id1) {
//			foreach($all as $id => $leadid1){
	//			if($leadid1 == $id1){
		//		$lead[$leadId] = $all[$leadid]; 
			//	}
	//	}}
		
		return $allLeadsArray;						
	}

	public static function getLeadsWithAll()
	{
		$allleads = self::getLeads();
		$allleads[-1] = 'All Leads';
		
		$allleads = array_reverse ( $allleads, true );
		return $allleads;	
	}
	
	
	

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
}
