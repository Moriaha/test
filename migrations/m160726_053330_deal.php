<?php

use yii\db\Migration;

class m160726_053330_deal extends Migration
{
    public function up()
    {
        $this->createTable(
            'deal',
            [
                'id' => 'pk',
                'leadId' => 'integer',	
				'name'=>'string',
				'amount'=>'integer',	
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('deal');
        return false;
    }

}
