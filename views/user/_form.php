<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
	
	<?php //admin can update all password, and all can update own password- to implement delete commente and set comment on the roe below
	//if( (\Yii::$app->user->can('updateOwnPassword', ['user' =>$model]))||(\Yii::$app->user->can('updatePassword')) || ($model->isNewRecord) ){?>
	<?//= $form->field($model, 'password')->passwordInput(['maxlength' => true])?>
	<?php //} ?>
	
	
	<?= // if we implement password rule this will be in comment and the code above will not be in comment
	 $model->isNewRecord ? $form->field($model, 'password')->passwordInput(['maxlength' => true]) : ""?>
	
    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
	
	<?php if (\Yii::$app->user->can('createUser')) { ?>
	<?= $form->field($model, 'role')->dropDownList($roles) ?>		
	<?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	<?php var_dump(User::getRoles())  ?>	

</div>